﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Document_Management_System.Models
{
    public class Documents
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DocumentType DocumentType { get; set; }
        public int DocumentTypeId { get; set; }
        public string Subject { get; set; }
        public string Note { get; set; }
        public UserGroup TransferedTo { get; set; }
        public int TransferedToId { get; set; }
        public IdentityUser User { get; set; }
        public string UserId { get; set; }

    }
}
