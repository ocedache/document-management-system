﻿namespace Document_Management_System.Models
{
    public class DocumentType
    {
        public int Id { get; set; }
        public string DocumentTypeName { get; set; }
    }
}