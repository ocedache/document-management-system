﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Document_Management_System.Models
{
    public class UserGroup
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
    }
}
